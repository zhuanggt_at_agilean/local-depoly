const express = require('express');
const config = require('./config');
const command = require('./command');
const path = require('path');
const fs = require('fs');
const DbConfig = require('./model/dbConfig');
const redisTools = require('./redisTools');
const processRepo = require('./model/processRepo');

const port = 9527;

(() => {
    const dbPath = path.join(config.staticPath, 'db.json');
    if (!fs.existsSync(dbPath)) {
        fs.writeFileSync(dbPath, JSON.stringify([]));
    }
    const readDb = () => (JSON.parse(fs.readFileSync(dbPath).toString('utf8')) || []).map(p => new DbConfig(p));
    const writeDb = processes => fs.writeFileSync(path.join(config.staticPath, 'db.json'), JSON.stringify(processes));

    const app = express();
    app.use(express.json());

    app.post('/app/process/start', async (req, res) => {
        const { id, forceCoverRdb, empty } = req.body;
        const process = await processRepo.getById(id);
        redisTools.checkConfFile(process);
        if (process.running && process.pid) {
            if (empty) {
                // 备份
                await redisTools.backup(process);
            }
            await command(`kill -9 ${process.pid}`);
        }
        if (forceCoverRdb) {
            redisTools.forceCoverRdbFile(process);
        }
        if (empty) {
            // 删库
            redisTools.empty(process);
        }
        await command(`${config.redisHome}/src/redis-server ${process.conf}`);
        res.send(JSON.stringify({ message: 'success' }));
    });

    app.post('/app/process/stop', async (req, res) => {
        const { id, save } = req.body;
        const process = await processRepo.getById(id);
        if (process.running && process.pid) {
            if (save) {
                await redisTools.backup(process);
            }
            await command(`kill -9 ${process.pid}`);
        }
        res.send(JSON.stringify({ message: 'success' }));
    });

    app.route('/app/processes')
        .get(async (req, res) => {
            const processes = await processRepo.list();
            res.send(JSON.stringify(processes));
        })
        .post(async (req, res) => {
            const body = req.body;
            console.log(JSON.stringify(body));
            const processes = readDb();
            const ret = new DbConfig({
                id: new Date().getTime(),
                baseDir: body.baseDir,
                port: body.port,
                rdbSource: body.rdbSource
            });
            processes.push(ret);
            writeDb(processes);
            res.send(JSON.stringify(ret));
        });
    app.use('/app', express.static(config.staticPath));
    app.listen(port, () => {
        console.log(`Example app listening at http://localhost:${port}/app`);
    });
})();