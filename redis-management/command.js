const cp = require('child_process');


module.exports = command => {
    return new Promise((resolve, reject) => {
        console.log('running:', command);
        cp.exec(command, (err, stdout) => {
            if (err) {
                console.log(err);
                return;
            }
            return resolve(stdout);
        });
    });
};