const path = require('path');
const fs = require('fs');
const { cat } = require('../support');
const config = require('../config');
const command = require('../command');

const tpl = cat(path.join(config.staticPath, 'redis.conf.template'));

const generate = (tpl, obj) => {
    let ret = tpl;
    for (let [k, v] of Object.entries(obj)) {
        ret = ret.replace(`%${k}%`, v);
    }
    return ret;
};

function displayAttr(m) {
    return (m <= 9 ? '0' + m : m)
};

function dateToYMD(date) {
    const [y, m, d, h, min, sec] = [
        date.getFullYear(),
        date.getMonth() + 1,
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds()
    ].map(displayAttr);
    return `${y}-${m}-${d}_${h}:${min}:${sec}`;
};

module.exports = {
    backup: async process => {
        await command(`${config.redisHome}/src/redis-cli -a ${process.password} -p ${process.port} save`);
        const backupDest = `${dateToYMD(new Date())}.rdb`;
        await command(`cp ${process.dumpFile} ${path.join(process.baseDir, backupDest)}`);
    },
    empty: ({
        port,
        baseDir,
        pidfile,
        logfile,
        conf,
        dumpFile
    }) => {
        if (fs.existsSync(dumpFile)) {
            console.log('delete ', dumpFile);
            fs.unlinkSync(dumpFile);
        }
    },
    forceCoverRdbFile: ({
        rdbSource,
        dumpFile
    }) => {
        if (!rdbSource) {
            return;
        }
        if (fs.existsSync(dumpFile)) {
            fs.unlinkSync(dumpFile);
        }
        if (rdbSource.startsWith('oss://')) {
            // TODO copy

        } else {
            fs.copyFileSync(rdbSource, dumpFile);
        }
    },
    checkConfFile: ({
        port,
        baseDir,
        pidfile,
        logfile,
        conf
    }) => {
        if (!fs.existsSync(baseDir)) {
            fs.mkdirSync(baseDir);
        }
        if (!fs.existsSync(conf)) {
            fs.writeFileSync(
                conf,
                generate(tpl, {
                    port,
                    baseDir,
                    logfile,
                    pidfile
                })
            );
        }
    }
}