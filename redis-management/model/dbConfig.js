const fs = require('fs');
const path = require('path');
const prettyBytes = require('pretty-bytes');

const cat = f => {
    if (fs.existsSync(f)) {
        return fs.readFileSync(f).toString('utf8');
    }
    return null;
};

module.exports = class DbConfig {
    constructor({
        port,
        rdbSource,
        baseDir,
        id,
        remark,
        password
    }) {
        this.id = id;
        this.remark = remark;
        this.baseDir = baseDir;
        this.port = port;
        this.rdbSource = rdbSource;
        this.password = password;
        this.conf = path.join(baseDir, 'redis.conf');
        this.logfile = path.join(baseDir, 'redis.log');
        this.pidfile = path.join(baseDir, 'redis.pid');
        this.dumpFile = path.join(baseDir, 'dump.rdb');
        if (fs.existsSync(this.dumpFile)) {
            this.dumpFileSize = prettyBytes(fs.statSync(this.dumpFile).size);
        } else {
            this.dumpFileSize = prettyBytes(0);
        }
        this.pid = cat(this.pidfile);
        this.running = false;
    }
};