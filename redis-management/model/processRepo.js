const path = require('path');
const fs = require('fs');

const config = require('../config');
const command = require('../command');

const DbConfig = require('./dbConfig');

const readDb = () => (JSON.parse(fs.readFileSync(path.join(config.staticPath, 'db.json')).toString('utf8')) || []).map(p => new DbConfig(p));

const listActive = async () => {
    let commandResult = await command("ps -ef | grep redis-server");
    const lines = commandResult.split('\n');
    return lines
        .filter(line => line.length > 0)
        .filter(line => !line.includes('grep'))
        .map(line => line.split(' ').filter(i => i !== ''))
        .filter(arr => arr && arr.length !== 0);
};

const bindInfo = activeProcessesInfo => activeProcessesInfo[activeProcessesInfo.length - 1];

const matchBindInfo = (process, activeProcessesInfo) => {
    for (activeProcessInfo of activeProcessesInfo) {
        console.log('bind info: ', bindInfo(activeProcessInfo));
        if (bindInfo(activeProcessInfo).includes(`${process.port}`)) {
            return true;
        }
    }
    return false;
};

const list = async () => {
    const processes = readDb();
    const activeProcessesInfo = await listActive();
    console.log('activeProcessesInfo', activeProcessesInfo);
    for (process of processes) {
        if (matchBindInfo(process, activeProcessesInfo)) {
            process.running = true;
        }
    }
    return processes;
};

const getById = async id => {
    const processes = readDb();
    const [process] = processes.filter(obj => obj.id === id);
    const activeProcessesInfo = await listActive();
    if (matchBindInfo(process, activeProcessesInfo)) {
        process.running = true;
        return process;
    }
    return process;
};

module.exports = {
    list,
    getById
};