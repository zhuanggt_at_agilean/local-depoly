function request({ method, url, body }) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.open(method, url, true);
        request.setRequestHeader('Content-type', 'application/json');
        request.onreadystatechange = function () {
            if (request.readyState === XMLHttpRequest.DONE) {
                var status = request.status;
                if (status === 0 || (status >= 200 && status < 400)) {
                    var result = JSON.parse(request.responseText);
                    resolve(result);
                } else {
                    reject(new Error('status: ' + status));
                }
            }
        };
        if (body) {
            request.send(JSON.stringify(body));
        } else {
            request.send();
        }
    });
};

const state = {
    processes: []
};

function renderTable() {
    const displaySetting = {
        pid: '进程id',
        running: '操作',
        port: '端口',
        baseDir: '基础目录',
        rdbSource: '使用的rdb',
        dumpFileSize: '库大小',
        remark: '备注'
    };
    const processes = state.processes;
    const table = document.getElementById('process_table');
    const lines = table.querySelectorAll(".process_detail");
    for (let line of lines) {
        table.removeChild(line);
    }
    const tHeader = document.createElement('thead');
    const standardKeys = Object.keys(processes[0]).filter(k => displaySetting[k]);
    standardKeys.map(k => {
        const th = document.createElement('th');
        th.innerText = displaySetting[k];
        return th;
    }).forEach(th => tHeader.appendChild(th));
    table.appendChild(tHeader);
    processes
        .map(process => {
            const tr = document.createElement('tr');
            for (let k of standardKeys) {
                const v = process[k];
                console.log(k, v);
                const td = document.createElement('td');
                if (k === 'running') {
                    const button = document.createElement('button');
                    if (v) {
                        button.innerText = 'stop';
                        button.onclick = () => {
                            request({
                                method: 'post',
                                url: '/app/process/stop',
                                body: {
                                    id: process.id,
                                    save: true
                                }
                            })
                        }
                    } else {
                        button.innerText = 'start';
                        button.onclick = () => {
                            request({
                                method: 'post',
                                url: '/app/process/start',
                                body: {
                                    id: process.id
                                }
                            })
                        }
                    }
                    td.appendChild(button);
                    tr.appendChild(td);
                } else if (k === 'rdbSource') {
                    const button = document.createElement('button');
                    const span = document.createElement('span');
                    if (v) {
                        button.innerText = '覆盖重启';
                        button.onclick = () => {
                            request({
                                method: 'post',
                                url: '/app/process/start',
                                body: {
                                    id: process.id,
                                    forceCoverRdb: true
                                }
                            })
                        }
                        span.innerText = v;
                    } else {
                        button.innerText = '置空库重启';
                        button.onclick = () => {
                            request({
                                method: 'post',
                                url: '/app/process/start',
                                body: {
                                    id: process.id,
                                    empty: true
                                }
                            })
                        }
                        span.innerText = "空库";
                    }
                    td.appendChild(span);
                    td.appendChild(button);
                    tr.appendChild(td);
                } else {
                    td.innerText = v;
                    tr.appendChild(td);
                }
            }
            return tr;
        })
        .forEach(n => {
            table.appendChild(n);
        });
};

window.onload = async () => {
    const processes = await request({
        method: 'get',
        url: 'processes',
    });
    console.log('here', processes);
    state.processes = processes;
    renderTable();

    const [
        portInput,
        dirInput,
        rdbInput,
        addButton
    ] = ['port', 'dir', 'rdb', 'add'].map(id => document.getElementById(id));
    addButton.addEventListener("click", async () => {
        const [port, baseDir, rdbSource] = [portInput, dirInput, rdbInput].map(ele => ele.value);
        console.table({ port, baseDir, rdbSource });
        const createResult = await request({
            method: 'POST',
            url: 'processes',
            body: {
                port, dir, rdb
            }
        });
        console.log(createResult);
        state.list.push(createResult);
    });
};