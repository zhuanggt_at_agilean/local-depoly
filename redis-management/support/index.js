const fs = require('fs');

const cat = f => {
    if (fs.existsSync(f)) {
        return fs.readFileSync(f).toString('utf8');
    }
    return null;
};

module.exports = {
    cat
};