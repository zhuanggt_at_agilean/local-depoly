#!/bin/bash

#memory=256m
#memory=512m
memory=1G

jarFileName=multi.jar

function build () {
    cd $KANBAN_ROOT/multi-function-server
    mvn clean install -DskipTests
    cp multi-main/target/multi-main-0.0.1.jar $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName}
}

function debug () {
    java -Xdebug -Xrunjdwp:transport=dt_socket,address=10003,server=y,suspend=y -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/multi-apollo.properties
}

function run () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx$memory -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/multi-apollo.properties> /dev/null 2>&1 &
}

function stop () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName};
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac
