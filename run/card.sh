# memory=256m
# memory=512m
memory=2G

jarFileName=card.jar

function build () {
    cd $KANBAN_ROOT/value-unit-server
    mvn clean install -DskipTests
    cp target/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName}
}

function debug () {
    java -Xdebug -Xrunjdwp:transport=dt_socket,address=10005,server=y,suspend=y -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/card-apollo.properties
}

function run () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx$memory -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/card-apollo.properties> /dev/null 2>&1 &
}

function stop () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName};
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac