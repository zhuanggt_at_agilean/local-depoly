

MODULE_PATH=user-entry-leave-server
jarFileName=uel.jar

function build () {
    cd $KANBAN_ROOT/$MODULE_PATH
    mvn clean install -DskipTests
    cp target/$jarFileName $KANBAN_NOHUP_RUN_JAR_DIR/$jarFileName
}

function run () {
    pid=$(ps -ef | grep $jarFileName | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx1G -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/$jarFileName --spring.config.location=${KANBAN_CONFIG_PATH}=user-leave-and-enter-apollo.properties,shared-apollo.properties,oauth-apollo.properties> /dev/null 2>&1 &
}

function stop () {
    pid=$(ps -ef | grep $jarFileName | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/$jarFileName $KANBAN_NOHUP_RUN_JAR_DIR/$jarFileName;
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac