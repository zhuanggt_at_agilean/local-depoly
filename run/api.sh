jarFileName=api.jar

function build() {
    cd $KANBAN_ROOT/value-kanban-domain
    mvn clean install -DskipTests

    cd $KANBAN_ROOT/value-kanban-shared
    mvn clean install -DskipTests

    cd $KANBAN_ROOT/open-beans
    mvn clean install -DskipTests

    cd $KANBAN_ROOT/openapi-client
    mvn clean install -DskipTests

    cd $KANBAN_ROOT/open-api
    mvn clean install -DskipTests
    cp target/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName}
}

function run() {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "kill pid: ${pid}"
    kill -9 $pid
    nohup java -Xmx1G -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/api-apollo.properties >/dev/null 2>&1 &
}

function stop() {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "kill pid: ${pid}"
    kill -9 $pid
}

function pull() {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName}
}

function debug () {
    java -Xdebug -Xrunjdwp:transport=dt_socket,address=10010,server=y,suspend=y -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/api-apollo.properties
}

function boot() {
    cd $KANBAN_ROOT/open-api
    mvn clean install -DskipTests
    cp target/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName}

    wait
    run
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"boot")
    boot
    ;;
"all")
    build
    run
    ;;
esac
