# memory=256m
memory=512m
#memory=1G

function build () {
    cd $KANBAN_ROOT/nstats-server
    mvn clean install -DskipTests
    cp target/nstats.jar $KANBAN_NOHUP_RUN_JAR_DIR/nstats.jar
}

function run () {
    pid=$(ps -ef | grep nstats.jar | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    config=${KANBAN_CONFIG_PATH}/nstats-apollo.properties,${KANBAN_CONFIG_PATH}/shardingsphere.yml
    nohup java -Xmx$memory -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/nstats.jar --spring.config.location=${config} > /dev/null 2>&1 &
    # java -Xmx1G -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/nstats.jar --spring.config.location=${KANBAN_CONFIG_PATH}/nstats-apollo.properties
}

function stop () {
    pid=$(ps -ef | grep nstats.jar | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/nstats.jar $KANBAN_NOHUP_RUN_JAR_DIR/nstats.jar;
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac