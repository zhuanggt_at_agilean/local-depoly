# memory=256m
memory=512m
#memory=1G

function build () {
    cd $KANBAN_ROOT/filter-server
    mvn clean install -DskipTests
    cp target/filter.jar $KANBAN_NOHUP_RUN_JAR_DIR/filter.jar
}

function run () {
    pid=$(ps -ef | grep filter.jar | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx$memory -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/filter.jar --spring.config.location=${KANBAN_CONFIG_PATH}/filter-apollo.properties> /dev/null 2>&1 &
}

function stop () {
    pid=$(ps -ef | grep filter.jar | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/filter.jar $KANBAN_NOHUP_RUN_JAR_DIR/filter.jar;
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac