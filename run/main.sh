
# memory=256m
memory=512m
#memory=1G

jarFileName=kanban.jar

function build () {
    cd $KANBAN_ROOT/value-auth
    mvn clean install -DskipTests
    cd $KANBAN_ROOT/server
    mvn clean install -DskipTests
    cp target/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName}
}

function debug () {
    java -Xdebug -Xrunjdwp:transport=dt_socket,address=10002,server=y,suspend=y -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/kb-apollo.properties
}

function run () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx$memory -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/kb-apollo.properties > /dev/null 2>&1 &
}

function stop () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName};
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac
