#!/bin/bash

jarFileName=history.jar

# memory=256m
memory=512m
#memory=1G

function build () {
    cd $KANBAN_ROOT/history-server
    mvn clean install -DskipTests
    cp target/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName}
}

function run () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx$memory -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/history-apollo.properties> /dev/null 2>&1 &
}

function debug () {
    java -Xdebug -Xrunjdwp:transport=dt_socket,address=10006,server=y,suspend=y -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/history-apollo.properties
}

function stop () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName};
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    ;;
esac