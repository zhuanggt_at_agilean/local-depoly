

jarFileName=notification-main-start.jar


function run () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx512M -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/notification-apollo.properties> /dev/null 2>&1 &
}

function stop () {
    pid=$(ps -ef | grep ${jarFileName} | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function build () {
    mkdir -p $KANBAN_NOHUP_RUN_JAR_DIR/plugins
    mkdir -p $KANBAN_NOHUP_RUN_JAR_DIR/pluginConfig
    cd $KANBAN_ROOT/notification
    mvn clean install -DskipTests
    cp $KANBAN_ROOT/notification/notification-main/target/notification-main-1.0-SNAPSHOT.jar $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName};
    cp $KANBAN_ROOT/notification/notification-plugins-parent/bot/target/bot-1.0-SNAPSHOT.jar $KANBAN_NOHUP_RUN_JAR_DIR/plugins/bot-1.0-SNAPSHOT.jar;
    cp $KANBAN_ROOT/notification/notification-plugins-parent/mail/target/mail-1.0-SNAPSHOT.jar $KANBAN_NOHUP_RUN_JAR_DIR/plugins/mail-1.0-SNAPSHOT.jar;
}

function pull () {
    mkdir -p $KANBAN_NOHUP_RUN_JAR_DIR/plugins
    mkdir -p $KANBAN_NOHUP_RUN_JAR_DIR/pluginConfig
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/${jarFileName} $KANBAN_NOHUP_RUN_JAR_DIR/${jarFileName};
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/mail-1.0-SNAPSHOT.jar $KANBAN_NOHUP_RUN_JAR_DIR/plugins/mail-1.0-SNAPSHOT.jar;
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/bot-1.0-SNAPSHOT.jar $KANBAN_NOHUP_RUN_JAR_DIR/plugins/bot-1.0-SNAPSHOT.jar;
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac