#!/bin/bash

#memory=256m
memory=512m
#memory=1G

jarFileName=oss.jar

function build () {
    cd $KANBAN_ROOT/oss-server
    mvn clean install -DskipTests
    cp target/oss.jar $KANBAN_NOHUP_RUN_JAR_DIR/oss.jar
}

function run () {
    pid=$(ps -ef | grep oss.jar | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
    nohup java -Xmx$memory -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/oss.jar --spring.config.location=${KANBAN_CONFIG_PATH}/oss-apollo.properties> /dev/null 2>&1 &
}

function debug () {
    java -Xdebug -Xrunjdwp:transport=dt_socket,address=10009,server=y,suspend=y -jar -Denv=local ${KANBAN_NOHUP_RUN_JAR_DIR}/${jarFileName} --spring.config.location=${KANBAN_CONFIG_PATH}/oss-apollo.properties
}

function stop () {
    pid=$(ps -ef | grep oss.jar | grep -v grep | awk '{print $2}')
    echo "pid is ${pid}"
    kill -9 $pid
}

function pull () {
    ossutil cp -f ${KANBAN_JAR_OSS_PATH}/oss.jar $KANBAN_NOHUP_RUN_JAR_DIR/oss.jar;
}

mode=$1

case $mode in
"pull")
    pull
    ;;
"run")
    run
    ;;
"build")
    build
    ;;
"debug")
    debug
    ;;
"stop")
    stop
    ;;
"all")
    build
    run
    ;;
esac
