### 设置本地的环境变量

- KANBAN_ROOT 本地后端代码库
- KANBAN_APP_ENV 包环境

```bash
export KANBAN_ROOT=/path/to/your/local/repo
export KANBAN_APP_ENV=${KANBAN_JAR_OSS_PATH}
```

### 每个脚本的命令参数

```bash
#编译
./card.sh build
#启动
./card.sh run
#停止
./card.sh stop
```